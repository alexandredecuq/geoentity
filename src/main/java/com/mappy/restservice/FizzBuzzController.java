package com.mappy.restservice;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class FizzBuzzController {

    static final String FIRST_MODULO_VALUE_ERROR = "firstModulo should be > 0";
    static final String SECOND_MODULO_VALUE_ERROR = "secondModulo should be > 0";
    static final String UPPER_BOUND_VALUE_ERROR = "upperBound should be > firstModulo and secondModulo";
    static final String CALL_TRACKER_IS_EMPTY = "callTracker for /api/fizz-buzz is empty";

    static HashMap<String, Integer> callTracker = new HashMap<>();

    @RequestMapping(value = "/api/fizz-buzz", method = {RequestMethod.GET})
    public String fizzBuzz(@RequestParam(value = "firstModulo") int firstModulo,
                             @RequestParam(value = "secondModulo") int secondModulo,
                             @RequestParam(value = "upperBound") int upperBound,
                             @RequestParam(value = "firstModuloString") String firstModuloString,
                             @RequestParam(value = "secondModuloString") String secondModuloString)
    {
        registerCall(firstModulo,secondModulo,upperBound,firstModuloString,secondModuloString);

        if(firstModulo <= 0)
            return FIRST_MODULO_VALUE_ERROR;
        if(secondModulo <= 0)
            return SECOND_MODULO_VALUE_ERROR;
        if(upperBound < firstModulo || upperBound < secondModulo)
            return UPPER_BOUND_VALUE_ERROR;

        return new FizzBuzz(firstModulo, secondModulo, upperBound, firstModuloString, secondModuloString).compute();
    }

    @RequestMapping(value = "/api/stats", method = {RequestMethod.GET})
    public String statistics()
    {
        if(callTracker.isEmpty())
            return CALL_TRACKER_IS_EMPTY;

        var entry = callTracker.entrySet()
                .stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .collect(Collectors.toList()).get(0);
        return "/api/fizz-buzz " + entry.getKey() + " called " + entry.getValue() + " time(s).";
    }

    private void registerCall(int firstModulo, int secondModulo, int upperBound, String firstModuloString,
                              String secondModuloString) {
        String key = firstModulo + "-" +
                secondModulo + "-" +
                upperBound + "-" +
                firstModuloString + "-" +
                secondModuloString;
        if(!callTracker.containsKey(key))
            callTracker.put(key, 0);
        Integer cnt = callTracker.get(key);
        callTracker.put(key, cnt+1);
    }
}
