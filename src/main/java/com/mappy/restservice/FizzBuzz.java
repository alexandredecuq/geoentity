package com.mappy.restservice;

public class FizzBuzz {
    private final int firstModulo;
    private final int secondModulo;
    private final int upperBound;
    private final String firstModuloString;
    private final String secondModuloString;

    public FizzBuzz(int firstModulo, int secondModulo, int upperBound, String firstModuloString, String secondModuloString) {
        this.firstModulo = firstModulo;
        this.secondModulo = secondModulo;
        this.upperBound = upperBound;
        this.firstModuloString = firstModuloString;
        this.secondModuloString = secondModuloString;
    }

    public String compute() {
        StringBuilder strBuilder = new StringBuilder();
        for(int i=1; i <= upperBound; i++) {
            if(i % (firstModulo * secondModulo) == 0)
                strBuilder.append(firstModuloString).append(secondModuloString);
            else if(i % firstModulo == 0)
                strBuilder.append(firstModuloString);
            else if(i % secondModulo == 0)
                strBuilder.append(secondModuloString);
            else
                strBuilder.append(i);
            if(i < upperBound)
                strBuilder.append(",");
        }
        return strBuilder.toString();
    }
}
