package com.mappy.restservice;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static com.mappy.restservice.FizzBuzzController.CALL_TRACKER_IS_EMPTY;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class FizzbuzzApplicationTests {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	void statsApiWorksCorrectly() {
		final String baseUrl = "http://localhost:" + port + "/api";
		String fizzBuzzUrl1 = "/fizz-buzz?firstModulo=3&secondModulo=5&upperBound=15&firstModuloString=fizz&secondModuloString=buzz";
		String fizzBuzzUrl2 = "/fizz-buzz?firstModulo=3&secondModulo=4&upperBound=15&firstModuloString=fizz&secondModuloString=buzz";

		assertThat(this.restTemplate.getForObject(baseUrl+"/stats", String.class))
				.contains(CALL_TRACKER_IS_EMPTY);
		assertThat(this.restTemplate.getForObject(baseUrl+fizzBuzzUrl1, String.class))
				.contains(FizzBuzzTests.expectedResult);
		assertThat(this.restTemplate.getForObject(baseUrl+"/stats", String.class))
				.contains("/api/fizz-buzz 3-5-15-fizz-buzz called 1 time(s).");
		assertThat(this.restTemplate.getForObject(baseUrl+fizzBuzzUrl2, String.class))
				.contains("1,2,fizz,buzz,5,fizz,7,buzz,fizz,10,11,fizzbuzz,13,14,fizz");
		assertThat(this.restTemplate.getForObject(baseUrl+fizzBuzzUrl2, String.class))
				.contains("1,2,fizz,buzz,5,fizz,7,buzz,fizz,10,11,fizzbuzz,13,14,fizz");
		assertThat(this.restTemplate.getForObject(baseUrl+"/stats", String.class))
				.contains("/api/fizz-buzz 3-4-15-fizz-buzz called 2 time(s).");
	}

}
