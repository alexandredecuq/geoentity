package com.mappy.restservice;

import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

public class FizzBuzzControllerTests {
    @Test
    void shouldReturnFirstModuloError() {
        String result = new FizzBuzzController().fizzBuzz(-1, 5, 15, "fizz", "buzz");
        Assert.isTrue(result.equals(FizzBuzzController.FIRST_MODULO_VALUE_ERROR),
                result + " != " + FizzBuzzController.FIRST_MODULO_VALUE_ERROR);
    }

    @Test
    void shouldReturnSecondModuloError() {
        String result = new FizzBuzzController().fizzBuzz(3, 0, 15, "fizz", "buzz");
        Assert.isTrue(result.equals(FizzBuzzController.SECOND_MODULO_VALUE_ERROR),
                result + " != " + FizzBuzzController.SECOND_MODULO_VALUE_ERROR);
    }

    @Test
    void shouldReturnUpperBoundError() {
        String result = new FizzBuzzController().fizzBuzz(3, 5, 4, "fizz", "buzz");
        Assert.isTrue(result.equals(FizzBuzzController.UPPER_BOUND_VALUE_ERROR),
                result + " != " + FizzBuzzController.UPPER_BOUND_VALUE_ERROR);
    }
}
