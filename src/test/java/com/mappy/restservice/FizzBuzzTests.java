package com.mappy.restservice;

import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

public class FizzBuzzTests {

    static final String expectedResult = "1,2,fizz,4,buzz,fizz,7,8,fizz,buzz,11,fizz,13,14,fizzbuzz";

    @Test
    void shouldComputeCorrectly() {
        FizzBuzz fizzBuzz = new FizzBuzz(3,5,15,"fizz","buzz");
        String result = fizzBuzz.compute();
        Assert.isTrue(expectedResult.equals(result), result + " != " + expectedResult);
    }
}
